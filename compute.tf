data "google_compute_zones" "available" {
  project = data.google_project.project.project_id
}

resource "google_compute_instance" "es_node" {
  count        = var.es_nodes_count
  project      = data.google_project.project.project_id
  zone         = data.google_compute_zones.available.names[0]
  name         = "elasticsearch-${count.index}"
  machine_type = var.machine_type

  tags = [
    "terraform",
    "elasticsearch"
  ]

  boot_disk {
    initialize_params {
      image = var.so_image
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }

  depends_on = [google_project_service.service]
}

output "es_node_id" {
  value = google_compute_instance.es_node.*.self_link
}

output "es_node_ip" {
  value = google_compute_instance.es_node.*.network_interface.0.network_ip
}
