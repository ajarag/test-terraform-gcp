data "google_project" "project" {
}

resource "google_project_service" "service" {
  for_each = toset([
    "compute.googleapis.com"
  ])
  service            = each.key
  project            = data.google_project.project.project_id
  disable_on_destroy = false
}

output "project_id" {
  value = data.google_project.project.project_id
}
