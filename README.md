# Instructions to test

Create a Compute Engine instance with *"Allow full access to all Cloud APIs"* option checked. Leave other options with their default values: **us-central1-a**, **e2-medium**, **Debian 10**, ...

Connect to the instance via SSH and [install Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli) with the Ubuntu/Debian package
```bash
sudo apt-get update && sudo apt-get install -y software-properties-common git
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform
terraform --version
```

Enable APIs with gcloud
```bash
gcloud services enable cloudresourcemanager.googleapis.com
```

Download the Terraform test code and apply it
```bash
git clone https://gitlab.com/ajarag/test-terraform-gcp.git
cd test-terraform-gcp
terraform init
terraform apply -var="so_image=debian-cloud/debian-10" -auto-approve
```

It should create the instances fine. Now destroy and create them again, but using the Drawfork images
```bash
terraform destroy -auto-approve
terraform apply -var="so_image=eip-images/debian-10-drawfork" -auto-approve
```
You should be able to reproduce the API returning the permission error reading the `eip-images` project.
```
Error 403: Required 'compute.images.get' permission for 'projects/eip-images/global/images/debi
an-10-drawfork'
```

> You can see more verbose output with `TF_LOG=DEBUG` or `TF_LOG=TRACE`
```bash
TF_LOG=DEBUG terraform apply -var="so_image=eip-images/debian-10-drawfork" -auto-approve
```

With gcloud I get a similar error
```bash
gcloud compute instances create drawfork-instance \
--image-project eip-images --image-family debian-10-drawfork
Did you mean zone [us-central1-a] for instance: [drawfork-instance] 
(Y/n)?  
ERROR: (gcloud.compute.instances.create) Could not fetch resource:
 - Required 'compute.images.useReadOnly' permission for 'projects/eip-images/global/images/debian-10-drawfork-v20200916'
```
