variable "es_nodes_count" {
  description = "Number of Elasticsearch nodes"
  default     = 3
}

variable "project_id" {
  description = "Google Cloud project ID"
}

variable "region" {
  description = "GCP Region where will be the project resources"
}

variable "so_image" {
  description = "Default OS distribution image"
}

variable "machine_type" {
  description = "Google Compute Engine machine type"
}
